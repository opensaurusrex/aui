# 9.5.0

* [Documentation](https://aui.atlassian.com/aui/9.5/)
* [Jira issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.5.0)

## Added
* `aui-item-link` supports `extra-classes` attribute;
  CSS classes provided in the attribute are reflected in the underlying anchor element (MNSTR-6095)

## Changed
* within the responsive app header component: custom CSS classes from the original dropdown trigger are now
  copied to the responsive trigger created inside the "More" dropdown (MNSTR-6095)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Accessibility improvement for avatar (AUI-5318)
* Makes chevron glyph consistent across dropdowns (AUI-5417)
* Support blanket showing when there are non-HTML elements (e.g. SVG nodes) in the body of the `document` (AUI-5429)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Updated the open version to 8.4.0 (BSP-3634)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)
* Updated engine-io to 6.1.3 (BSP-3719)
* Excluded dom4j dependency (BSP-3722)

## Added

* Avatar group component (AUI-4676)
* Avatar component and avatar badged component (AUI-4484)
* Added `lazyLoadImage` attribute to `avatar.soy` template (MNSTR-5545)
* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)
* Added `enable` and `disable` options to `tooltip()` (AUI-5428)

### Atlassian-Plugin
# 9.4.4

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.4)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Update open version to 8.4.0 (BSP-3634)
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated normalize-url to 4.5.1 (BSP-3753)
* Updated commons-io to 2.7 (BSP-3767)
* Reply CSS classes from the original app header dropdown trigger to the new responsive trigger within the "More" dropdown (MNSTR-6095) (it's an opt-in behaviour in this bugfix AUI version)

# 9.4.3

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.3)

* Re-run release

# 9.4.2 - botched release

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.2)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)

# 9.4.1

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.1)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)
* Makes chevron glyph consistent across dropdowns (AUI-5417)

## Added

* Removing the message from DOM after being visually hidden (AUI-5103)
* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)
* Added `enable` and `disable` options to `tooltip()` (AUI-5428)

# 9.4.0

* [Documentation](https://aui.atlassian.com/aui/9.4/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.4.0)

## Added

* Closing of any layered element can be prevented (AUI-5273)

## Changed

* Updated build dependencies
    * Compiled using webpack 5 (AUI-5331)
    * Updated autoprefixer 9.8.6 -> 10.2.5
    * Updated cssnano 4.10.0 -> 5.0.2
    * Updated postcss 7.0.25 -> 8.2.15
* Forms with `form.aui` CSS styles are now using `static` positioning instead of `relative`. This fixes an issue with
  inline-dialogs rendering incorrectly when placed inside forms that are inside dialogs (AUI-5353)
* `z-index` values revised on several components.
    * See the `Removed` section for components where explicit `z-index` values were removed.
    * AUI sidebar `z-index` decreased to `1` down from `3`. (AUI-5330)
    * AUI Tooltip `z-index` decreased to `5000` down from `9999`.
    * AUI Select2 `z-index` decreased to `3001` down from `9999`.

## Removed

* Explicit `z-index` values were removed from the following components:
    * AUI application header (was `4`). (AUI-5330)
    * AUI Inline Dialog (was `100`, but effectively `3000` when opened by layer manager).
    * Closeable AUI messages (was `4000`).

### Atlassian-Plugin

# 9.3.12

## Fixed
* Reply CSS classes from the original app header dropdown trigger to the new responsive trigger within the "More" dropdown (MNSTR-6095)

  (it's an opt-in behaviour in this bugfix AUI version)

# 9.3.11

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.11)

## Fixed
* Fix the empty tooltip shown for empty title (AUI-5434)
* Modify the tooltip component to avoid makes a redundant DOM update and title function call if passed (AUI-5433)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated qs to 6.5.2 or higher (BSP-3666)

# 9.3.10 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.10)

## Fixed
* Fix the empty tooltip shown for empty title (AUI-5434)
* Modify the tooltip component to avoid makes a redundant DOM update and title function call if passed (AUI-5433)

# 9.3.9

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.9)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Updated unset-value to 2.0.1 (BSP-3668)

# 9.3.8 - failed release
# 9.3.11

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.11)

## Fixed
* Fix the empty tooltip shown for empty title (AUI-5434)
* Modify the tooltip component to avoid makes a redundant DOM update and title function call if passed (AUI-5433)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated commons-io to 2.7 (BSP-3767)

# 9.3.10 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.10)

## Fixed
* Fix the empty tooltip shown for empty title (AUI-5434)
* Modify the tooltip component to avoid makes a redundant DOM update and title function call if passed (AUI-5433)

# 9.3.9

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.9)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Updated unset-value to 2.0.1 (BSP-3668)

# 9.3.8 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.8)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)

# 9.3.7

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.7)

* Re-run release

# 9.3.6 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.6)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)

# 9.3.5

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.5)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)
* Date picker support for `minDate` and `maxDate` options (AUI-5376)
* CalendarWidget is properly exposed under `window.AJS` in `aui.prototyping.js` and `aui.prototyping.nodeps.js` (AUI-5377)
* The `.tooltip()` won't throw an error when it cannot find the trigger in the document (AUI-5401)
* Added `enable` and `disable` options to `tooltip()` (AUI-5428)
* Makes chevron glyph consistent across dropdowns (AUI-5417)

## Deprecated

* The alignment arrows on layered components (`aui-inline-dialog2`, `aui-dropdown2`) are going to be removed in AUI 10.0.0 (AUI-5170)
  * The undocumented `aui-dropdown2-tailed` CSS class prints deprecation warning

## Added

* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)

# 9.3.4

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.4)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

# 9.3.3

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.3)

## Fixed

* AUI icons in layers nested in application header will not be set to 24px in size. (AUI-5221, AUI-5368)

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.3.2

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.2)

## Changed

* Icons in `.aui-header-before` section reduced in size from 24px to 16px (AUI-5368)
* Tooltip timeout reduced from 500ms to 300ms (AUI-5369).

## Fixed

* Badges have a border again when placed on sidebar (AUI-5346)
* Avatars have the correct size in layered elements on the header (AUI-5348)
* Radio and checkbox form fields style are applied to direct label only (AUI-5343)
* `.assistive` properties are declared with `!important` rule (AUI-5343)
* Content in `.aui-header-before` section receives correct text color (AUI-5368)
* Tooltips no longer add scrollbars to pages after first appearance on the page (AUI-5366)

# 9.3.1

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.1)

## Changed

* All layered elements - Dropdown, Inline Dialog, Modal Dialog, Flag - explicitly set a number of inheritable CSS
  properties in order to prevent undesirable cascades: `color`, `font-variant`, `font-style`, `font-weight`,
  `letter-spacing`, `line-height`, `text-align`, and `white-space`.

## Fixed

* Text inside Inline dialogs nested in AUI application header have correct body text color (AUI-5337).
* Links inside Inline dialogs nested in AUI application header have correct link color (AUI-5337).
* Dropdowns and Inline Dialogs nested in AUI table headers have correct font weight (AUI-5339).
* Dropdowns and Inline Dialogs nested in AUI toolbars wrap text correctly (AUI-5344).
* Global browser focus ring styles are no longer included by default (AUI-5325)
* Tooltip styles now match ADG (AUI-5350)
* Selec2 clear icon shows correctly (AUI-5342).
* Select2 has the correct ADG dropdown styles (AUI-5345)

## Deprecated

### Atlassian-Plugin

* The `jQuery().tipsy()` function and styles are available via a `com.atlassian.auiplugin:jquery-tipsy` web-resource.
  This will be removed in a future major version.

# 9.3.0

* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.0)

## Changed

* Swap jQuery.tipsy with Popper.js to handle AUI tooltips (AUI-5253)
* Updated underscore 1.10.2 -> 1.12.0
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)


# 9.2.8

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.8)

## Fixed
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 3.2.2, 4.4.14, 5.0.6, 6.1.1 (BSP-3669)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)

# 9.2.8

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.8)

## Fixed
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)


# 9.2.8

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.8)

## Fixed
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 3.2.2, 4.4.14, 5.0.6, 6.1.1 (BSP-3669)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)

# 9.2.8

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.8)

## Fixed
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)

# 9.2.8

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.8)

## Fixed
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)

# 9.2.7

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.7)

## Fixed
* Asynchronous initialisation of AUI header (AUI-5432). See AUI header documentation for details.
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Updated unset-value to 2.0.1 (BSP-3668)

# 9.2.6

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.6)

* Re-run release

# 9.2.5 - failed release

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.5)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Prevent AUI overriding the `AJS.I18n` after WRM has set it, for compatibility with the two phase JS I18n transform (AUI-5431)

# 9.2.4

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.4)

## Fixed

* Makes chevron glyph consistent across dropdowns (AUI-5417)

## Added

* Added auiSelectField Soy template as a type of `aui.forms.field` in `form.soy` (MNSTR-5602)

# 9.2.3

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.3)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)

## Added

* Added `lazyLoadImage` attibute to `avatar.soy` template (MNSTR-5545)

# 9.2.2

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.2)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

## Fixed

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.2.1

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.1)

## Changed

* Built using Node v12
* Production dependency versions are pinned, so that they do not change between builds
    * jQuery Tipsy version bumped 1.3.1 -> 1.3.3

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)
* Aligned underline for link button that uses text and icon (AUI-5306)
* Fixed XSS vulnerability in Tabs component (AUI-5080)
* Tabbing order in AUI dialog2 component now takes into account iframes (AUI-5290)

# 9.2.0

* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.0)

## Added

* Sortable Table now accepts options via its JavaScript API. See the documentation for details.
* Badges have three new variants: `.aui-badge-added`, `.aui-badge-removed`, and `.aui-badge-important`.

## Changed

* Bumped build and minifier dependencies.
* The navigation items CSS pattern now accepts arbitrary elements as children, not just `<span>` or `<a>` elements.

## Fixed

* Date picker's background colour is correct in light mode.
* Select2's background color is set correctly.
* Chevrons do not repeat on standard `<select>` elements.

## Removed

* The `Navigation#hideMoreItems` method has been removed -- it has not worked for years and nobody seemed to mind ;)

### Atlassian-Plugin

# 9.1.10

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.10)

## Fixed
* Vulnerability in jquery-ui used by aui (VULN-627502, VULN-627503, VULN-627504)
* Vulnerability of XSS fix in jquery-form component (VULN-539557)
* Updated css-what version to 5.0.1 (BSP-3269)
* nth-check npm version upgraded to 2.0.1 (BSP-3270)
* Updated ajv from 5.5.2 to 6.12.3 (BSP-3266)
* Updated ssri version to 6.0.2 (BSP-3505)
* Updated ansi-regex version to 5.0.1 (BSP-3268)
* Update lodash version to 4.17.21 (BSP-3635)
* Update open version to 8.4.0 (BSP-3634)
* Updated unset-value to 2.0.1 (BSP-3668)
* Updated tar version to 6.1.10 (BSP-3741)
* Updated json-schema to 0.4.0 and wait-on to 6.0.1 (BSP-3716)
* Updated trim-newlines to 3.0.1 or 4.0.1 (BSP-3670)
* Updated marked to 4.0.12 (BSP-3740)
* Updated qs to 6.5.2 or higher (BSP-3666)
* Updated plist to 3.0.4 (BSP-3675)
* Updated commons-io to 2.7 (BSP-3767)

# 9.1.9

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.9)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)
* Makes chevron glyph consistent across dropdowns (AUI-5417)

# 9.1.8

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.8)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

## Fixed

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.1.7

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.7)

## Changed

* Built using Node v12
* Updated build dependencies
    * Updated autoprefixer 6.7.7 -> 6.8.9
    * Updated cssnano 3.10.0 -> 4.1.10
    * Updated postcss 5 -> 7.0.35
    * Replaced uglifyjs-webpack-plugin with terser-webpack-plugin 4.2.3
* Production dependency versions are pinned, so that they do not change between builds
    * jQuery Tipsy version bumped 1.3.1 -> 1.3.3

# 9.1.6

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.6)

## Changed

* Compiled with babel v7.13

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)
* Aligned underline for link button that uses text and icon (AUI-5306)
* Fixed XSS vulnerability in Tabs component (AUI-5080)

# 9.1.5

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.5)

## Fixed

* Tabbing order in AUI dialog2 component now takes into account iframes (AUI-5290)

# 9.1.4

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.4)

## Fixed

* Missing accessibility features in sidebar.soy template (AUI-5244)
* Application header styles for logo match other header items (AUI-5276)

# 9.1.3

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.3)

## Fixed

* Fixed XSS vulnerability in Dropdown2.

# 9.1.2

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.2)

## Fixed

* Form fields elements do not render double-sized focus-ring in browsers that natively support :focus-visible
* Expander component behaves properly when trying to toggle multiple times

## Changed

### Documentation

* AUI Documentation now properly describes multiple ways to consume AUI

### Single Select

* aui-option uses encodeURI for img-src escaping, allowing for usage of URLs with additional parameters and/or special
  characters

# 9.1.1

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.1)

## Fixed

* Trigger component no longer causes DOM exception when there are cross-origin iframes in the document

# 9.1.0

* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.0)

## Fixed

### Dropdown

* Dropdown aligns properly when rendered in legacy Edge.

## Changed

* AUI's version of `Popper.js` has been bumped to v2.4.4, up from v1.16.1.
* layered components expose data-popper-placement attribute instead of x-placement as their internal API. x-placement
  attribute has been DEPRECATED and is kept for backwards compatibility only.

# 9.0.7

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.7)

## Fixed

* Date picker sets proper value when underlying input has `type=date` (AUI-5380)

# 9.0.6

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.6)

## Changed

* Updated underscore 1.10.2 -> 1.13.1. See https://underscorejs.org/#changelog for full list of changes in underscore.

## Fixed

### Atlassian-Plugin

* Fixed translations in `AJS.whenIType` keyboard shortcuts module. (AUI-5370)

# 9.0.5

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.5)

## Changed

* Built using Node v12
* Updated build dependencies
    * Updated autoprefixer 6.7.7 -> 6.8.9
    * Updated cssnano 3.10.0 -> 4.1.10
    * Updated postcss 5 -> 7.0.35
    * Replaced uglifyjs-webpack-plugin with terser-webpack-plugin 4.2.3
* Production dependency versions are pinned, so that they do not change between builds
    * jQuery Tipsy version bumped 1.3.1 -> 1.3.3

# 9.0.4

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.4)

This version contains all changes from up to 7.9.11 that were skipped in 7.10.1

## Changed

* Compiled with babel v7.13

## Fixed

* RestfulTable autoFocus on CreateRow event not turning off (AUI-5048)
* AUI Toggle component rendering in IE11 (AUI-5074)

## Security

* XSS vulnerability in dropdown component (AUI-5275)
* All XSS vulnerabilities fixed up to 7.9.11

# 9.0.3

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.3)

## Fixed

### Documentation

* Contrast on elements in FlatApp has been corrected

### Select component

* Chevrons in select fields were adjusted for dark mode

### Layered components

* All layered components (i.e.: Dropdown) position correctly if loaded inside iFrame.

# 9.0.2

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.2)

## Changed

### Date Picker component

* AUI date picker widgets can be destroyed and re-created

## Fixed

### Dropdown component

* Buttons in Dropdown items span the Dropdown's width properly

# 9.0.1

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.1)

## Changed

### Toggle button component

* Documentation now correctly states that label is accepted as attribute only.
* It used to state that the component may accept label as attribute and property. It was never true.

### Documentation

* Dark mode toggle is now available across all our demo and test apps.

## Fixed

### Flag component

* Flag uses legacy-browsers friendly way of looping trough NodeList

### Toggle button component

* Toggle buttons should retain their icon size in more places.

# 9.0.0

* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.0)

## Highlights

* AUI no longer supports IE 11.
* AUI now has dark mode support!
* AUI's layered components have been audited and updated to improve their accessibility.
* AUI's patterns now include skip-links and landmarks for better screen reader and keyboard navigation support - they
  are applayed on docs website as well.
* All patterns planned to be removed in 9.0.0 will be removed in 10.0.0 instead.

## Added

* Dark mode for all AUI components and patterns. It can be enabled by adding an `aui-theme-dark` CSS class to `<body>`.
* A new skip link pattern has been added and documented.
* A new `.aui-close-button` pattern has been added. All elements that can be closed now use this pattern.
* All focusable elements have an explicit `:focus` and `:focus-visible` style by default.
* Form fields auto-filled by the browser have a new custom style.
* All layered elements may specify a dom container that will be used as appending point upon render.
    * Read the AUI 9 upgrade documentation for details on how to make best use of this property.

## Changed

* AUI's version of `Underscore` has been bumped to v1.10.2, up from v1.9.2.
* All AUI components should meet WCAG 2.0 AA colour contrast rates between text and background colours when used with
  AUI's page layout patterns.
* Layered components are considered closed by default. They are visible once an `open` attribute is added to them.
* Layered components reset their font-size to `1rem` and `text-align` to `left`.
* Layered components, except Dialog2, are no longer appended to the body.
* Animations for layered components are based on the presence or absence of an `open` attribute. Changing the `hidden`
  attribute is immediate and does not animate.
* Callbacks registered using a Dialog2 instance's `on` and `off` methods will only trigger when that specific dialog
  fires the event, as opposed to nested components.
* The `.aui-page-header`, `.aui-group`, `.aui-buttons`, and other positional patterns now use CSS flexbox for layout.
* AUI's datepicker no longer displays the placeholder by default. The config API was extended to set the placeholder
  explicitly.

### Avatar component

* Avatar sizes are now controlled by the `--aui-avatar-size` CSS property.

### Button component

* Button colours are now controlled by the `--aui-btn-bg`, `--aui-btn-border`, and `--aui-btn-text` CSS properties.
    * Read the AUI button documentation for details on how to make best use of these properties.

### Dropdown component

* ARIA attributes are no longer given discrete styles. CSS classes or basic HTML elements should be used instead.
    * Replaced selection of `[role=menuitem]` with `a` and `button`.
    * Replaced selection of `[role=menuitemcheckbox]` and `[role=checkbox]` with `.aui-dropdown2-checkbox`.
    * Replaced selection of `[role=menuitemradio]` and `[role=radio]` with `.aui-dropdown2-radio`.

### Icon component

* Icons are now `display:flex`. Icon glyphs are centered within the icon's display box using `margin:auto`.
* Icon sizes are now set by the `--aui-icon-size` CSS property, initially set within `.aui-icon`.
* Icon glyphs are now set by the `--aui-ig` CSS property, initially set within `.aui-icon`.

### Message component

* Message dimensions are now set by `--aui-message-padding` and `--aui-message-gutter`, initially set on `.aui-message`.
* Message dimensions are now re-used and selectively overridden by the Flag and Banner patterns.

### Page CSS reset

* Base font-size is defined on `html` instead of `body`. This means `rem` units will reflect AUI's default font size.
* CSS normalization rules needed for IE9, IE10 and IE11 have been removed.
* The hidden attribute is now declared with `display: none !important `.

### Page layout

* Page layout has been updated to simplify the placement of sidebar, <main>, and other content regions.
* Page layout and application header patterns now document skip links that should be added to a page, such that
  keyboard-only users can quickly jump to specific page regions and important actions.
* If the page includes a sidebar, the `#content` element should have only 2 child elements:
    * `<section class="aui-sidebar" aria-label="sidebar">` - houses the sidebar and its content.
    * `<section aria-label="page">` - houses all other page content.
    * See the updated page layout documentation, or the upgrade guide, for more details.

### Sidebar

* We changed `a` element from extend button to actual `button` element.
* We now use iconfont `aui-iconfont-chevron-double-left` instead of custom icon for the extend button.
* Sidebar width is now set by the `--aui-sidebar-width` CSS property, initially set within `.aui-page-sidebar`.

## Fixed

* Any dropdown trigger element that references a non-existent element will no longer throw an error on click or hover.
* Any dropdown item element with the `aui-dropdown2-interactive` class will *always* prevent default for any event
  triggered on it.
    * This is in line with the intent and behaviour described in the documentation.
    * Previously, events would be passed through if the item was either checked or disabled.
* Opening a dropdown via the down arrow no longer causes the document scroll position to jump.
* Layer positioning accounts for triggers inside iframe containers and should position themselves correctly.
* Date picker's year suffix will no longer render as the literal string "null" by default.
* Layered element's shadows should be visible in Edge.

### Accessibility

* All invalid usage of `aria-hidden` has been removed.
    * Read the upgrade guide to determine whether you need to make similar changes to your markup or usages of AUI
      components.
* All close buttons in all AUI components are part of the tab order and have a consistent label of "Close".
* Modal dialog accessibility is improved:
    * Modal dialogs are given keyboard focus and announced to screen readers when opened.
    * The documentation now outlines how to give a modal dialog an appropriate accessible label.
* Inline dialog accessibility is improved for toggle behaviour:
    * The contents of an open inline dialog can now be navigated to using only the keyboard.
    * Inline dialog element is now focused after opening.
    * The documentation now outlines how to give an inline dialog an appropriate accessible label. Note that the hover
      behaviour variant of inline dialog is inaccessible and is now deprecated.
* Application header menu accessibility is improved:
    * The pattern's `<nav>` element must now be labelled as "site".
    * The site logo is now a `<span>` instead of an `<h1>`. The `<h1>` is reserved for the page's main content heading.
* Dropdown component accessibility is improved:
    * Incorrect use of `role=application` and `role=presentation` have been removed.
    * The styles for hover and focus are visually distinct, making it easier for people using a keyboard and mouse
      simultaneously to tell what item has keyboard vs. mouse focus.
    * The first focusable item of a dropdown is focussed on opening the dropdown. This eases access for NVDA and JAWS
      users.
    * Dropdown group headings will be announced as focus moves in to the group. The headings are no longer navigable by
      screen reader's virtual cursors.
* Message pattern accessibility is improved:
    * Link and button text colours are now set to the message's standard text colour and are given an underline, to
      ensure colour contrast ratios are met.
    * Focus indicators are set to the message text colour, to ensure contrast ratios are met.

## Deprecated

* Inline dialog 2 hover functionality is deprecated and will be removed in the future.
* populateParameters function and params object is deprecated and will be removed in AUI 10.0.0.
* AUI's legacy dropdown1 component has been extracted to its own `@atlassian/aui-legacy-dropdown` package.
* AUI's legacy toolbar1 component has been extracted to its own `@atlassian/aui-legacy-toolbar` package.

## Removed

* All Atlassian brand logos have been removed. You can access them from
  the [@atlassian/brand-logos](https://www.npmjs.com/package/@atlassian/brand-logos) package.
* The `.aui-legacy-focus` class support has been removed. All focusable elements should have an appropriate focus ring
  applied.
* Message icons - we removed following classes
    * .aui-message .icon-close
    * .aui-icon-close
    * .aui-message .icon-close-inverted,
    * .aui-message.error .icon-close,
    * .aui-icon-close-inverted The `.aui-close-button` pattern should be used instead.
* jQuery ajaxSettings configuration was removed. Please specify it globaly for product or add it per request where
  needed.

# Previous versions

- [8.x.x](./changelogs/8.x.x.md)
- [7.x.x](./changelogs/7.x.x.md)
- [6.x.x](./changelogs/6.x.x.md)
- [5.x.x](./changelogs/5.x.x.md)
- [4.x.x](./changelogs/4.x.x.md)
