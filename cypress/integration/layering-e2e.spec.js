export default () => {
    describe('deeply nested layers', function () {
        describe('dropdowns inside modal dialogs', function () {
            beforeEach(function () {
                cy.visit('layering/');
                cy.get('[data-testid="open-outer-dialog"]').click();
                cy.get('[data-testid="open-inner-dialog"]').click();
            });

            // TODO AUI-5378 fix opening by keyboard
            it.skip('can open submenus using the keyboard', function () {
                // open the first dropdown
                cy.get('[data-testid="open-dd-level-0"]')
                    .should('be.visible')
                    .focus()
                    .type('{enter}')
                    .get('#dd-level-0')
                    .should('be.visible');
                cy.focused()
                    .type('{downarrow}')
                    .get('#dd-level-0').should('be.visible');

                cy.focused()
                    .type('{rightarrow}')
                    .get('#dd-level-1').should('be.visible');

                cy.focused()
                    .type('{rightarrow}')
                    .get('#dd-level-1').should('be.visible')
                    .get('#dd-level-2').should('be.visible');
            });

            describe('with all submenus open', function () {
                beforeEach(function () {
                    cy.get('[data-testid="open-dd-level-0"]').click();
                    cy.get('[for=dd-level-1] > a').trigger('mouseover');
                    cy.get('[for=dd-level-2] > a').trigger('mouseover');
                });

                // TODO AUI-5378 implement test and fix behaviour
                it.skip('only closes innermost menu when moving back up one level', function () {
                });

                // TODO AUI-5378 implement test and fix behaviour
                it.skip('only closes menus when a submenu action is taken', function () {

                });
            });
        });
    });
};
