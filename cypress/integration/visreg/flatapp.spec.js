/// <reference types="Cypress" />
/* eslint-disable */

function visreg(items) {
    return items.map(id => ({ id, selector: `[data-visreg="${id}"]` }));
}

const variants = {
    'forms/default': visreg([
        'default-text-fields',
        'default-file-upload',
        'default-dropdowns-and-selects',
        'default-select-date',
        'default-textarea-legend',
        'default-checkboxes-edit',
        'default-checkboxes-legend',
        'default-one-checkbox',
        'default-legend-length-testing',
        'default-multi-column-checkboxes',
        'default-radio-buttons',
        'default-radio-matrix--deprecated',
        'default-buttons-container',
        'default-field-value-state',
        'default-checkboxes-state',
        'default-radio-buttons-state',
        'pre-text-area',
        'field-group-paragraph',
        'right-aligned-buttons--deprecated',
        'button-spacing',
        'field-icons',
        'soy-default',
        'soy-extra',
    ]),
    'forms/inlineForm': visreg([
        'inline-form',
    ]),
    'forms/topLabels': visreg([
        'top-labels-one',
        'top-label',
        'file-upload',
        'buttons-container',
        'checkboxes-edit',
        'one-checkbox',
        'legend-long-text',
        'radio-buttons'
    ]),
    'forms/inlineHelp': visreg([
        'text-entry',
        'file-upload',
        'dropdowns-and-selects',
        'date-select',
        'textarea-legend',
        'groups',
        'more-checkboxes',
        'one-checkbox',
        'multi-column-checkboxes',
        'radio-buttons',
        'radio-matrix',
        'text-entry-state',
        'checkboxes-view',
        'radio-buttons-state',
        'pre-text-area',
        'edit-state',
        'file-upload-state',
        'dropdowns-and-selects-state',
        'date-select-state',
        'textarea-legend-state',
        'checkboxes-edit-state',
        'checkboxes-legend-state',
        'one-checkbox-state',
        'checkbox-matrix-state',
        'radio-buttons-state-2',
        'radio-matrix-state-2',
        'dfv1-text',
        'dfv1-checkboxes',
        'dfv1-radio-buttons',
        'dfv1-pre-text-area',
        'dfv1-inline-form',
        'dfv1-top-label',
        'dfv1-checkboxes-legend',
        'dfv1-checkbox-legend',
        'dfv1-radio-buttons-long'
    ]),
    'forms/fieldsAndStates': visreg([
        'field-types',
        'disabled-styling',
        'field-widths-default',
        'field-widths-short',
        'field-widths-medium',
        'field-widths-long',
        'field-widths-fill-width',
    ]),
    'forms/dropDownConsistency': visreg([
        'drop-down-consistency'
    ]),
    'forms/auiSelectField': visreg([
        'aui-select-field'
    ])
};

const togglers = {
    '#switch-to-adg3': '---adg3',
    '#switch-to-legacy': '---legacy'
};
Object.entries(togglers).forEach(([query, suffix]) => {
    Object.entries(variants).forEach(([path, selectors]) => {
        context(path, () => {
            const pathPrefix = path.replace(/\//g, '_');

            beforeEach(() => {
                cy.visit(path);
                cy.get(query).click();
            });

            selectors.forEach(({ id, selector }) => {
                it(id, () => {
                    cy.get(selector).matchImageSnapshot(`${pathPrefix}_${id}${suffix}`)
                })
            });
        });
    });
});

it('experimental/select2', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const pathPrefix = path.replace(/\//g, '_');

    cy.visit(path);
    cy.get(selector).first().matchImageSnapshot(`${pathPrefix}_${selector}`);
});

it('experimental/select2 - multiselect', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const multiselectSelector = '#s2id_aui-select2-single';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit(path);

    // Multiselect closed
    {
        cy.get(multiselectSelector).first().matchImageSnapshot(`${pathPrefix}_${multiselectSelector}_closed`);
    }

    // Multiselect open
    {
        cy.get(multiselectSelector).first().click();
        cy.focused().blur();
        cy.get(selector).matchImageSnapshot(`${pathPrefix}_${multiselectSelector}_open`);
    }

    // Multiselect with choice
    {
        const selectOffscreenSelector = '.select2-results';
        cy.get(selectOffscreenSelector).get('.select2-result-selectable').first().click();
        cy.focused().blur();
        cy.get(multiselectSelector).first().matchImageSnapshot(`${pathPrefix}_${selectOffscreenSelector}`);
    }
});

it('experimental/select2 - single select', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const containerSelector = '#s2id_aui-select2-single2';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit(path);

    // Single select closed
    {
        cy.get(containerSelector).matchImageSnapshot(`${pathPrefix}_${containerSelector}_closed`);
    }

    // Single select open
    {
        cy.get(containerSelector).first().click();
        cy.focused().blur();
        cy.get(selector).matchImageSnapshot(`${pathPrefix}_${containerSelector}_open`);
    }
});

it.skip('experimental/select2 - single select with allow clear', () => {
    const path = 'experimental/select2/';
    const selector = '.aui';
    const containerSelector = '#s2id_aui-select2-allow-clear';
    const pathPrefix = path.replace(/\//g, '_');
    cy.visit(path);

    // Single select closed
    {
        cy.get(containerSelector).first().matchImageSnapshot(`${pathPrefix}_${containerSelector}_closed`);
    }

    // Single select open
    {
        cy.get(containerSelector).first().click();
        cy.focused().blur();
        cy.get(selector).matchImageSnapshot(`${pathPrefix}_${containerSelector}_open`);
    }

    // Single select with choice and clear selection icon
    {
        const selectOffscreenSelector = '.select2-results';
        cy.get(selectOffscreenSelector).get('.select2-result-selectable').first().click();
        cy.focused().blur();
        cy.get(containerSelector).first().matchImageSnapshot(`${pathPrefix}_${containerSelector}_selected`);
    }
});

it('experimental/singleSelect', () => {
    const path = 'singleSelect';
    const selector = '.aui-page-panel-content';

    cy.visit(path);
    cy.get(selector).matchImageSnapshot(`${path}_${selector}`);
});

it('dropdown2/confined/', () => {
    const path = 'dropdown2/confined/';
    const pathPrefix = path.replace(/\//g, '_');
    const dropdown_1 = 'button[aria-controls="test-cropped"]';
    const dropdown_2 = 'button[aria-controls="test-visible"]';
    const dropdown_3 = 'button[aria-controls="test-visible-2"]';
    const selector = '#confined-container';

    cy.visit(path);

    cy.get(dropdown_1).first().click();
    cy.get(selector).matchImageSnapshot(`${pathPrefix}_${selector}_1`);

    cy.get(dropdown_2).first().click();
    cy.get(selector).matchImageSnapshot(`${pathPrefix}_${selector}_2`);

    cy.get(dropdown_3).first().click();
    cy.get(selector).matchImageSnapshot(`${pathPrefix}_${selector}_3`);
});

/*
 * AUI-5237
 * Be sure dropdowns loaded in iFrames work properly.
 * Tests a scenario when Layered Component and it's trigger are both located inside of iFrame.
 */
it('dropdown2/positioned/', () => {
    const path = 'dropdown2/positioned/';
    const pathPrefix = path.replace(/\//g, '_');
    const trigger = 'button[aria-controls="simple-dropdown"]';
    const dropdown = '#simple-dropdown';

    cy.visit(path);

    cy.frameLoaded('#dropdown-iframe');

    cy.iframe('#dropdown-iframe')
        .wait(1000) // because this iframe is extramely flaky without it
        .find(trigger)
        .focus()
        .click();

    // Make sure the dropdown actually opened...
    cy.iframe('#dropdown-iframe')
        .find(dropdown)
        .should('be.visible');

    // AUI-5237 - don't take screenshot of a specific element
    // or the iframe will reload?!
    cy.matchImageSnapshot(`${pathPrefix}_${trigger}_1`, {
        capture: 'viewport'
    });
});

it("experimental/tooltips", () => {
    const path = "experimental/tooltips";
    const pathPrefix = path.replace(/\//g, "_");

    const scrollContainerIntoView = visregContainerName => cy.get(`[data-visreg="${visregContainerName}"]`).scrollIntoView().wait(100);
    const captureTooltip = (idSelector) => {
        cy.get(idSelector).first()
            // we use jQuery trigger here to prevent cypress default scrolling on trigger call
            .then((el) => el.trigger("mouseover"));

        cy.get("#aui-tooltip:not(.assistive)")
            .should("be.visible").wait(100)
            .then(() => {
                const name = idSelector.slice(1); // to get rid of #
                cy.matchImageSnapshot(`${pathPrefix}_${name}`, {
                    capture: 'viewport',
                    clip: { x: 0, y: 0, width: 1050, height: 600 }

                });
            })
    }
    const captureTooltipWithinContainer = (triggerIdSelector, visregContainerName) => {
        scrollContainerIntoView(visregContainerName)
        captureTooltip(triggerIdSelector)
    }

    cy.visit(path);
    cy.viewport(1100, 400);

    captureTooltipWithinContainer('#simple-tooltip', 'simple-tooltip-container');

    captureTooltipWithinContainer('#avatar-person', 'avatar-tooltip-container');

    captureTooltipWithinContainer('#custom-tooltip', 'custom-tooltip-container');

    // Gravity tooltips
    const gravityContainerName = 'gravity-tooltip-container'
    captureTooltipWithinContainer('#north-west', gravityContainerName);
    captureTooltipWithinContainer('#north', gravityContainerName);
    captureTooltipWithinContainer('#north-east', gravityContainerName);
    captureTooltipWithinContainer('#west', gravityContainerName);
    captureTooltipWithinContainer('#east', gravityContainerName);
    captureTooltipWithinContainer('#south-west', gravityContainerName);
    captureTooltipWithinContainer('#south', gravityContainerName);
    captureTooltipWithinContainer('#south-east', gravityContainerName);

    captureTooltipWithinContainer('#spacing-tooltip-1', 'spacing-tooltip-container');
    captureTooltipWithinContainer('#spacing-tooltip-2', 'spacing-tooltip-container');

    // Live tooltips
    captureTooltipWithinContainer('#live-tooltip-1', 'live-tooltip-container');
    captureTooltipWithinContainer('#live-tooltip-2', 'live-tooltip-container');
    captureTooltipWithinContainer('#live-tooltip-3', 'live-tooltip-container');
    captureTooltipWithinContainer('#live-tooltip-4', 'live-tooltip-container');

    captureTooltipWithinContainer('#full-docs', 'styled-tooltip-container');
});

// AUI-5348 - don't crop avatars in dropdowns/inline dialogs
it('experimental/auiHeader/avatars', () => {
    cy.visit('experimental/pageLayout/header/auiHeader/');

    cy.get("[href='#nav4-dropdown2-header1']").should("be.visible")
        .matchImageSnapshot('aui-5348_avatar_in_header_nav');

    cy.get("#aui-5348-trigger")
        .click()
        .get("#aui-5348-inline-dialog").wait(100).should("be.visible")
        .matchImageSnapshot('aui-5348_avatars_in_header_layers');
});

context("layer/integrations", () => {
    it("inline-dialog2 in application header", () => {
        cy.visit("experimental/pageLayout/header/auiHeader/interop/");

        cy.get("#test-aui5337")
            .click()
            .get("#dialog-in-app-header-1").wait(100).should("be.visible")
            .matchImageSnapshot('layerintegrations_appheader_inline-dialog2');
    });

    it("layers in flow content", () => {
        cy.visit("layering/");

        cy.get("[data-testid='test-aui5339-dropdown']")
            .click()
            .get("#aui5339-dropdown").wait(100).should("be.visible")
            .matchImageSnapshot('layerintegrations_flowcontent_dropdown');

        cy.get("[data-testid='test-aui5339-inline-dialog']")
            .click()
            .get("#aui5339-inline-dialog").wait(100).should("be.visible")
            .matchImageSnapshot('layerintegrations_flowcontent_inline-dialog2');
    });

    it("layers in toolbar2", () => {
        cy.visit("layering/");

        cy.get("[data-testid='test-aui5344-dropdown']")
            .click()
            .get("#aui5344-dropdown").wait(100).should("be.visible")
            .matchImageSnapshot('layerintegrations_toolbar2_dropdown');

        cy.get("[data-testid='test-aui5344-inline-dialog']")
            .click()
            .get("#aui5344-inline-dialog").wait(100).should("be.visible")
            .matchImageSnapshot('layerintegrations_toolbar2_inline-dialog2');
    });
});

it('forms/auiSelectField', () => {
    const path = 'forms/auiSelectField';
    const pathPrefix = path.replace(/\//g, '_');
    const selectSection = '#aui-select-field-test';
    const auiSelect = '#aui-select-field';
    const dropdownOptions = '.aui-optionlist';

    cy.visit(path);

    //auiSelectField is closed
    {
        cy.get(selectSection).matchImageSnapshot(`${pathPrefix}_${selectSection}_closed`);
    }

    // auiSelectField is opened
    {
        cy.get(auiSelect).first().click();
        cy.get(selectSection).matchImageSnapshot(`${pathPrefix}_${selectSection}_open`);
    }

    // auiSelectField- The first option out of eight was selected
    {
        cy.get(auiSelect).first().click();
        cy.get(dropdownOptions).children().should('have.length', 8);
        cy.get(dropdownOptions).children().first().click();
        cy.get(selectSection).matchImageSnapshot(`${pathPrefix}_${selectSection}`);
    }
})
