const { run } = require('../runner');

const baseUrl = 'http://localhost:7000/';
const paths = [
    'experimental/buttons/',
    'dropdown2/newer/',
    'sidebar/'
];

run({ name: 'flatapp', baseUrl, paths });
