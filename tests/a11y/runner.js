const pa11y = require('pa11y');
const mocha = require('mocha');
const {assert} = require('chai');

const defaultPa11yOpts = {
    runners: ['axe', 'htmlcs'],
    standards: ['Section508', 'WCAG2A', 'WCAG2AA'],
    chromeLaunchConfig: {
        args: [
            // because we run in a docker image using the 'root' user.
            // see https://github.com/puppeteer/puppeteer/issues/3698
            '--no-sandbox',
        ]
    }
};

function generateTests(url, err, results = {}) {
    const {issues} = results;

    mocha.describe(url, function() {
        mocha.it('runs successfully', function() {
            assert.equal(err, null);
        });

        if (issues) {
            mocha.describe('errors', function() {
                issues.forEach(issue => {
                    const {runner, code, selector, context, message } = issue;
                    mocha.it(`[${runner}] ${code} @ \`${selector}\``, function() {
                        let description = `\`${context}\` - ${message}`;
                        assert.fail(1, 2, description);
                    });
                })
            });
        }
    });
}

function run(config, options = {}) {
    const pa11yOpts = Object.assign({}, defaultPa11yOpts, options);
    const { name, baseUrl, paths } = config;
    mocha.describe(`a11y - ${name}`, function() {
        this.timeout(10000);

        paths.forEach(path => {
            const url = baseUrl + path;

            mocha.describe(url, function() {
                mocha.before(function() {
                    return pa11y(url, pa11yOpts)
                        .then(results => generateTests(url, null, results))
                        .catch(err => generateTests(url, err));
                });

                // A hackish way to ensure our `before` block runs and generates our actual tests.
                mocha.it('was checked', function() {
                    assert.ok(true);
                });
            });
        });
    });

}

module.exports = {
    run,
};
