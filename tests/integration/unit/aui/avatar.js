import $ from '@atlassian/aui/src/js/aui/jquery';
import '@atlassian/aui/src/js/aui/avatar';
import '@atlassian/aui/src/js/aui/avatar-group';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { fixtures, afterMutations } from '../../helpers/all';


describe('aui/avatar' , () => {
    let avatarEl;
    let avatarGroupEl;
    let avatarSelector = 'aui-avatar';

    const createAvatarEl = (html) => {
        avatarEl = fixtures({
            avatar: `${html}`
        }).avatar;
        skate.init(avatarEl);
    }

    const createAvatarGroupEl = (html) => {
        avatarGroupEl = fixtures({
            avatarGroup: `${html}`
        }).avatarGroup;
        skate.init(avatarGroupEl);
    }

    describe('avatar visible', () => {
        beforeEach(function() {
            createAvatarEl('<aui-avatar></aui-avatar>');
        });

        it('The avatar should be appended to DOM', () => {
            expect($(avatarSelector).length).to.equal(1);
        });

        it('The avatar should be visible', () => {
            expect($(avatarSelector).is(':visible')).to.equal(true);
        });

        it('The tag:<aui-avatar> and his first child should have the aui-avatar class', () => {
            expect($('.aui-avatar').length).to.equal(2);
        });
    });

    describe('avatar sizes', () => {
        beforeEach(function() {
            createAvatarEl('<aui-avatar size="medium"></aui-avatar>');
        });

        it('The avatar should has aui-avatar-${size} class', () => {
            const avatarInnerSpan = avatarEl.querySelector('.aui-avatar-medium');

            expect(avatarInnerSpan).to.be.instanceof(HTMLElement);
            expect(avatarInnerSpan.classList.contains('aui-avatar-medium')).to.equal(true);
        });

        it('The Avatar size attribute should exists', () => {
            expect(avatarEl.hasAttribute('size')).to.equal(true);
            expect(avatarEl.getAttribute('size')).to.equal('medium');
        });

        it('The avatar size should be changed', () => {
            expect(avatarEl.getAttribute('size')).to.equal('medium');
            afterMutations(() => {
                avatarEl.setAttribute('size', 'large');
                expect(avatarEl.getAttribute('size')).to.equal('large');
            });
        });
    });

    describe('The avatar type', () => {
        beforeEach(function() {
            createAvatarEl('<aui-avatar type="project"></aui-avatar>');
        });

        it('The avatar should has aui-avatar-${type} class', () => {
            const avatarInnerSpan = avatarEl.querySelector('.aui-avatar-project');

            expect(avatarInnerSpan).to.be.instanceof(HTMLElement);
            expect(avatarInnerSpan.classList.contains('aui-avatar-project')).to.equal(true);
        });

        it('The avatar should has type attribute', () => {
            expect(avatarEl.hasAttribute('type')).to.equal(true);
            expect(avatarEl.getAttribute('type')).to.equal('project');
        });

        it('The avatar size should be changed', () => {
            expect(avatarEl.getAttribute('type')).to.equal('project');

            avatarEl.setAttribute('type', 'user');
            expect(avatarEl.getAttribute('type')).to.equal('user');
        });
    });

    describe('The avatar with image', () => {
        let avatarImage;

        beforeEach(() => {
            createAvatarEl('<aui-avatar src="https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg" title="avatar-image" alt="customer-avatar"></aui-avatar>');

            avatarImage = avatarEl.querySelector('img');
        });

        it('The avatar should has inner img element', () => {
            expect(avatarImage).to.be.instanceOf(HTMLElement);
        });

        it('The avatar inner img should has src attribute', () => {
            expect(avatarImage.getAttribute('src')).to.equal('https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg');
        });

        it('The avatar inner img should has alt attribute', () => {
            expect(avatarImage.getAttribute('alt')).to.equal('customer-avatar');
        });

        it('The avatar inner img should has title attribute', () => {
            expect(avatarImage.getAttribute('title')).to.equal('avatar-image');
        });
    });

    describe('The avatar badged',  () => {
        let badgedWrapperElement;

        it('The avatar should has img badged', () => {
            createAvatarEl(`<aui-avatar>
            <aui-avatar-badged>
               <img src="https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg" />
            </aui-avatar-badged>
         </aui-avatar>`);

            badgedWrapperElement = avatarEl.querySelector('.aui-avatar-badged');

            expect(badgedWrapperElement).to.be.instanceOf(HTMLElement);
            expect(badgedWrapperElement.querySelector('img').getAttribute('src')).to.equal('https://icon-library.com/images/avatar-icon-images/avatar-icon-images-4.jpg');
        });

        it('The avatar should has svg badged', () => {
            createAvatarEl(`<aui-avatar>
            <aui-avatar-badged>
               <svg height="100%" version="1.1" viewBox="0 0 8 8" width="100%" fill="#FF5630" xmlns="http://www.w3.org/2000/svg">
                  <description>Online</description>
                  <circle cx="4" cy="4" r="4"></circle>
               </svg>
            </aui-avatar-badged>
         </aui-avatar>`);

            badgedWrapperElement = avatarEl.querySelector('.aui-avatar-badged');

            expect(badgedWrapperElement).to.be.instanceOf(HTMLElement);
            expect(badgedWrapperElement.querySelector('svg')).to.be.instanceOf(SVGElement);
        });

        describe('Avatar badged position', () => {
            beforeEach(() => {
                createAvatarEl(`<aui-avatar>
               <aui-avatar-badged>
                  <svg height="100%" version="1.1" viewBox="0 0 8 8" width="100%" fill="#FF5630" xmlns="http://www.w3.org/2000/svg">
                     <description>Online</description>
                     <circle cx="4" cy="4" r="4"></circle>
                  </svg>
               </aui-avatar-badged>
            </aui-avatar>`);

                badgedWrapperElement = avatarEl.querySelector('.aui-avatar-badged');
            });

            it('The avatar badged has default position', () => {
                expect(badgedWrapperElement.getAttribute('placement')).to.equal('bottom-end');
                expect(badgedWrapperElement.classList.contains('aui-avatar-badged-bottom-end')).to.equal(true);
            });

            it('The avatar badged has attribute placement(placement should set position)', () => {
                badgedWrapperElement.setAttribute('placement', 'top-start');

                afterMutations(function() {
                    expect(badgedWrapperElement.getAttribute('placement')).to.equal('top-start');
                    expect(badgedWrapperElement.classList.contains('aui-avatar-badged-top-start')).to.equal(true);
                });
            });
        });
    });

    describe('Avatar group', () => {
        let fixture;
        let avatarGroup;
        let avatarGroupSelector = 'aui-avatar-group';


        describe('Default avatar group', () => {
            beforeEach(() => {
                createAvatarGroupEl(`<aui-avatar-group>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
            </aui-avatar-group>`)
                fixture = document.querySelector('#test-fixture');
                avatarGroup =  fixture.querySelector(avatarGroupSelector);
            });

            it('The avatar group is visible in DOM', () => {
                expect(avatarGroup.classList.contains('aui-avatar-group')).to.equal(true);
            });

            it('The avatar group includes 3 avatar items', () => {
                expect(avatarGroup.querySelectorAll(avatarSelector).length).to.equal(3);
            });

            it('The avatar group has a default size', () => {
                expect(avatarGroup.getAttribute('size')).to.equal('medium');
                expect(avatarGroup.classList.contains('aui-avatar-group-medium')).to.equal(true);
            });

            it('The avatar group inner items should have a size equal parentNode size', () => {
                expect(avatarGroup.querySelector(avatarSelector).hasAttribute('size')).to.equal(true);
                expect(avatarGroup.querySelector(avatarSelector).getAttribute('size')).to.equal('medium');
            });
        });

        describe('Make some changes to avatar group', () => {
            beforeEach(() => {
                createAvatarGroupEl(`<aui-avatar-group size="large">
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
            </aui-avatar-group>`)
                fixture = document.querySelector('#test-fixture');
                avatarGroup =  fixture.querySelector(avatarGroupSelector);
            });

            it('The avatar group size should change with large to small', () => {
                expect(avatarGroup.getAttribute('size')).to.equal('large');
                avatarGroup.setAttribute('size', 'small');

                afterMutations(() => {
                    expect(avatarGroup.getAttribute('size')).to.equal('small');
                });
            });

            it('The avatar group has a class equal size attribute', () => {
                expect(avatarGroup.classList.contains('aui-avatar-group-large')).to.equal(true);
            });

            it('The avatar group inner items should change size', () => {
                expect(avatarGroup.querySelector(avatarSelector).getAttribute('size')).to.equal('large');
                avatarGroup.setAttribute('size', 'small');

                afterMutations(() => {
                    expect(avatarGroup.querySelector(avatarSelector).getAttribute('size')).to.equal('small');
                });
            });
        });

        describe('The avatar group badged', () => {
            beforeEach(() => {
                createAvatarGroupEl(`<aui-avatar-group>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
               <aui-avatar></aui-avatar>
            </aui-avatar-group>`)
                fixture = document.querySelector('#test-fixture');
                avatarGroup =  fixture.querySelector(avatarGroupSelector);
            });

            it('The avatar group badged should be visible', () => {
                expect(avatarGroup.querySelectorAll('.aui-avatar-group-badged').length).to.equal(1);
            });

            it('The avatar group badged should be remove if aui-avatar inner avatars group is <= 4', () => {
                const avatarsItem = avatarGroup.querySelectorAll(avatarSelector);

                expect(avatarGroup.querySelectorAll('.aui-avatar-group-badged').length).to.equal(1);
                avatarsItem[0].remove();

                afterMutations(() => {
                    expect(avatarGroup.querySelectorAll('.aui-avatar-group-badged').length).to.equal(0);
                });
            });
        });
    });
});
