AJS.$(function() {
    // BEGIN -- Dialog/Inline dialog2 focus -- BEGIN
    AJS.$('#make-me-javascripty').auiSelect2();
    AJS.$('#dialog-focus-show-button').on('click', function() {
        AJS.dialog2('#dialog-focus-large').show();
    });

    AJS.$('#dialog-focus-large-close-button').on('click', function() {
        AJS.dialog2('#dialog-focus-large').hide();
    });
    // END -- Dialog/Inline dialog2 focus -- END

    AJS.$('#dialog2-dropdown-stacking-show-button').on('click', function() {
        AJS.dialog2('#dialog2-dropdown-stacking').show();
    });
    AJS.$('#dialog-stacking-show-button').on('click', function() {
        AJS.dialog2('#dialog-stacking-large').show();
    });

    AJS.$('#dialog-stacking-large-open-button').on('click', function() {
        AJS.dialog2('#dialog-stacking-medium').show();
    });

    AJS.$('#dialog-stacking-large-close-button').on('click', function() {
        AJS.dialog2('#dialog-stacking-large').hide();
    });

    // Open / close for medium
    AJS.$('#dialog-stacking-medium-open-button').on('click', function() {
        AJS.dialog2('#dialog-stacking-small').show();
    });

    AJS.$('#dialog-stacking-medium-close-button').on('click', function() {
        AJS.dialog2('#dialog-stacking-medium').hide();
    });

    // Close for small
    AJS.$('#dialog-stacking-small-close-button').on('click', function() {
        AJS.dialog2('#dialog-stacking-small').hide();
    });

    // Dialog/Inline dialog2 stacking
    AJS.$('#dialog-inline-dialog2-stacking-show-button').on('click', function() {
        AJS.dialog2('#dialog-inline-dialog2-stacking-large').show();
    });
    AJS.$('#dialog-inline-dialog2-stacking-large-close-button').on('click', function() {
        AJS.dialog2('#dialog-inline-dialog2-stacking-large').hide();
    });

    AJS.$('#dialogx2-inline-dialog2-stacking-show-button').on('click', function() {
        AJS.dialog2('#dialogx2-inline-dialog2-stacking-large').show();
    });
    AJS.$('#dialogx2-stacking-large-open-button').on('click', function() {
        AJS.dialog2('#dialogx2-2-inline-dialog2-stacking-large').show();
    });

    // Content dialog behaviours
    AJS.$(document).on('click', '#dialog-with-lots-of-content-button', function (e) {
        AJS.dialog2('#dialog-with-lots-of-content').show();
        e.preventDefault();
    });
    AJS.$(document).on('click', '#dialog-with-lots-of-content-close-button', function (e) {
        AJS.dialog2('#dialog-with-lots-of-content').hide();
        e.preventDefault();
    });
    var sizeOptions = ['small', 'medium', 'large', 'xlarge', '\u26C4\uFE0F'];
    sizeOptions.forEach(function(size) {
        var option = '<div class="field-group"><label><input type="radio" name="content-dialog-size" value="' + size + '" />' + size + '</label></div>';
        AJS.$('#change-content-dialog-size').append(option);
    });
    AJS.$(document).on('submit', '#change-content-dialog-size', function(e) { e.preventDefault(); });
    AJS.$(document).on('change', '#change-content-dialog-size input', function(e) {
        var value = e.target.value;
        var $dialog = AJS.$('#dialog-with-lots-of-content');
        $dialog.removeClass(sizeOptions.map(function(size) {
            return 'aui-dialog2-' + size;
        }).join(' '));
        $dialog.addClass('aui-dialog2-' + value);
    });

    // Dialog with changed primary button order
    AJS.$(document).on('click', '#dialog-with-changed-focus-order-open-button', function () {
        AJS.dialog2('#dialog-with-changed-focus-order').show();
    });
    AJS.$(document).on('click', '#dialog-with-changed-focus-order-cancel-button, #dialog-with-changed-focus-order-confirm-button', function () {
        AJS.dialog2('#dialog-with-changed-focus-order').hide();
    });


    // Dialog with form and inline-dialog
    AJS.$(document).on('click', '#dialog-with-form-and-inline-dialog-open-button', function () {
        AJS.dialog2('#dialog-with-form-and-inline-dialog').show();
    });
    AJS.$(document).on('click', '#dialog-with-form-and-inline-dialog-cancel-button, #dialog-with-form-and-inline-dialog-confirm-button', function () {
        AJS.dialog2('#dialog-with-form-and-inline-dialog').hide();
    });
});
