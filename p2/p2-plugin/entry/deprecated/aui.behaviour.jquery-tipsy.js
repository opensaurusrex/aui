import $ from 'jquery';
import '@atlassian/tipsy/src/stylesheets/tipsy.css';
import '@atlassian/tipsy/src/javascripts/jquery.tipsy.js';
export default $;
