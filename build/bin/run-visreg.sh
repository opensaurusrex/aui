#!/bin/sh
# Determine where the baseline images are. By default, it is assumed they exist
# on a branch with the same name as the one checked out in AUI.
# If you're in a PR on Bitbucket however, it will check for them at the PR's upstream
# branch's name.
./build/bin/prepare-visreg.sh
# Fail the visreg test suite early when preparation fails
if [ $? -ne 0 ]; then
    echo "Failing early. Probably atlassian/aui-visual-regression is missing a branch we want to use"
    exit 1
fi

# export DEBUG=cypress:*
yarn install --frozen-lockfile --non-interactive
yarn visreg/ci --ci-build-id ${BITBUCKET_BUILD_NUMBER}
# Passing exit code of the previous command as an argument to update-visreg.sh script
./build/bin/update-visreg.sh $?
