#!/bin/sh
echo "Running pre-commit hook:"
lint() {
    echo "Gathering files... "
    diff=$(git diff --cached --name-only --diff-filter=ACM | grep -E '(.js)$')
    if [ -z "$diff" ]; then
        echo "No JS changes found."
        exit 0
    fi
    files="$(echo $diff | tr '\n' ' ')"
    echo "Linting..."
    yarn eslint ${files}
    exit $?
}
lint
