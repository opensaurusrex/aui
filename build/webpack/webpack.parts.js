const path = require('path');
const { DefinePlugin } = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { cacheIdentity } = require('../lib/cacher');
const project = require('../../package.json');

const { NODE_ENV } = process.env;

const namedFilesCfg = {
    name: 'assets/[name].[ext]'
};

exports.setAuiVersion = () => ({
    plugins: [
        new DefinePlugin({
            AUI_VERSION: JSON.stringify(project.version || 'UNKNOWN')
        })
    ]
});

exports.transpileJs = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/i,
                include,
                exclude,
                use: {
                    loader: 'babel-loader',
                    options: Object.assign({
                        cacheDirectory: true,
                        cacheIdentifier: cacheIdentity(options),
                        configFile: path.resolve(__dirname, '../../babel.config.js')
                    }, options),
                },
            }
        ]
    }
});

exports.transpileJsWithCodeCoverage = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.js$/i,
                include,
                exclude,
                use: {
                    loader: 'babel-loader',
                    options: Object.assign({
                        cacheDirectory: true,
                        cacheIdentifier: cacheIdentity(options),
                        configFile: path.resolve(__dirname, '../../babel.config.js')
                    }, options),
                },
            },
            {
                test: /\.js$/i,
                include,
                exclude: [
                    /(entry|node_modules|js-vendor|tests|\.spec\.ts$)/,
                ],
                enforce: 'post',
                use: {
                    loader: '@jsdevtools/coverage-istanbul-loader',
                    options: {
                        compact: true,
                        esModules: true
                    }
                },
            },
        ]
    }
});

exports.extractCss = ({ include, exclude, options } = {}) => {
    const extractCSS = new MiniCssExtractPlugin(options);

    return {
        module: {
            rules: [
                {
                    test: /\.(?:less|css)$/i,
                    include,
                    exclude,

                    use: [
                        MiniCssExtractPlugin.loader,
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                postcssOptions: {
                                    plugins: [
                                        'autoprefixer'
                                    ]
                                }
                            }
                        },
                        'less-loader'
                    ]
                }
            ],
        },

        plugins: [
            extractCSS
        ],
    }
};

exports.inlineCss = () => {
    return {
        module: {
            rules: [
                {
                    test: /\.(?:less|css)$/i,
                    use: [
                        'style-loader',
                        'css-loader',
                        'less-loader'
                    ]
                }
            ]
        }
    };
};

exports.loadFonts = ({ include, exclude, options } = {}) => {
    const opts = Object.assign({}, namedFilesCfg, options);
    return {
        module: {
            rules: [
                {
                    test: /\.(eot|otf|ttf|woff|woff2)$/,
                    include,
                    exclude,
                    use: {
                        loader: 'file-loader',
                        options: opts,
                    },
                }
            ]
        }
    };
};

exports.loadImages = ({ options } = {}) => {
    const opts = Object.assign({}, namedFilesCfg, options);
    let imageMatcher = /\.(png|jpg|gif|svg)$/;
    let brandAndIconMatcher = /[\\/](?:.*?-logos|logos|fonts)[\\/]/;

    return {
        module: {
            rules: [
                {
                    test: imageMatcher,
                    exclude: brandAndIconMatcher,
                    use: {
                        loader: 'url-loader',
                        options: opts,
                    },
                },
                {
                    test: imageMatcher,
                    include: brandAndIconMatcher,
                    use: {
                        loader: 'file-loader',
                        options: opts,
                    }
                }
            ],
        },
    };
};

exports.loadSoy = ({ include, exclude, options } = {}) => ({
    module: {
        rules: [
            {
                test: /\.soy$/,
                include,
                exclude,
                use: {
                    loader: '@atlassian/atlassian-soy-loader',
                    options,
                },
            },
        ]
    },
    plugins: [
        new DefinePlugin({
            'goog.DEBUG': false // will allow dead code removal of debugging code from generated templates
        })
    ]
});

exports.resolveSoyDeps = () => ({
    module: {
        rules: [
            {
                test: require.resolve('@atlassian/soy-template-plugin-js/src/js/atlassian-deps.js'),
                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'atl_soy'
                    }
                ]
            },
            {
                test: require.resolve('@atlassian/soy-template-plugin-js/src/js/soyutils.js'),
                use: [
                    {
                        loader: require.resolve('./loaders/expose-exports-loader'),
                        options: 'soy,soydata,goog'
                    }
                ]
            }
        ]
    },
});

exports.production = (mode = NODE_ENV) => {
    const definePlugin = new DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(mode)
        }
    });

    if (mode !== 'production') {
        return {
            mode: 'development',
            devtool: 'cheap-module-source-map',

            plugins: [
                definePlugin
            ]
        };
    }

    const TerserWebpackPlugin = require('terser-webpack-plugin');
    const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

    return {
        mode: 'production',
        devtool: 'source-map',

        optimization: {
            minimize: true,
            usedExports: false,

            minimizer: [
                new TerserWebpackPlugin({
                    parallel: true,
                    extractComments: false,
                    terserOptions: {
                        mangle: {
                            properties: false,
                            reserved: ['I18n']
                        },
                    },
                }),
                new CssMinimizerPlugin({
                    minimizerOptions: {
                        processorOptions: {
                            safe: true,
                            discardComments: {
                                removeAll: true
                            }
                        }
                    }
                }),
            ]
        },

        plugins: [
            definePlugin,
        ]
    }
};
