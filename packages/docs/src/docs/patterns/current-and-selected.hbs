---
component: Design pattern - current and selected items
pageCategory: patterns

analytics:
  pageCategory: patterns
  component: patterns-current-and-selected-item
---
<article>
    <h2>Summary</h2>
    <blockquote>
        <dfn>current</dfn>  <i>/ˈkɜr·ənt, ˈkʌr·ənt/</i><br />
        adjective (HAPPENING NOW) - of the present time or most recent
    </blockquote>

    <p>
        Whenever dealing with collections of items (i.e. lists, navigations), we may want to differentiate several items.<br/>
        Those items may be considered as active, selected or otherwise highlighted due to user’s action or location.
    </p>

    <h2>Common patterns</h2>
    <p>
        There are plenty of common patterns where a thing in a group<sup><a href="#fn1" id="r1" class="footnote"><span class="assistive">Footnote </span>[1]</a></sup> may be considered as selected<sup><a href="#fn2" id="r2" class="footnote"><span class="assistive">Footnote </span>[2]</a></sup>. To name few:<br/>
        <ul>
            <li>The current page/link in a navigation block</li>
            <li>The current time in a timetable/schedule</li>
            <li>The current date in a calendar</li>
            <li>The selected dates (date range) in a calendar</li>
            <li>The active tab in a horizontal navigation block</li>
            <li>The selected options in an option list</li>
        </ul>
    </p>

    <h2>Current vs selected</h2>
    <p>While being very similar in behaviour, current and selected items may carry different meanings and both can occur within the same set.</p>
    <p>What's the difference then? Let's have a look on some examples:</p>

    <p>
        In a navigation pattern <b>current</b> may be used to indicate which page is currently displayed (see: <a href="#current-page">current page</a>), while <b>selected</b> indicates which page will be displayed if the user activates the item.<br/>
        Furthermore, the same navigation tree may support operating on one or more selected items, for instance by using context menu containing options such as "delete" and "move".
    </p>
    <p>
        Similarly, in a calendar pattern <b>current</b> may be used to indicate the currently set system date, while <b>selected</b> indicates the date that will be used if the user activates the item.<br/>
        Moreover, the same calendar may support specifying more than one selected date, thus specifying date ranges.
    </p>

    <p>In principle</p>
    <div class="aui-message aui-message-info">
        <p><dfn id="def-current">Current item</dfn> can only exist as a <strong>single instance</strong> in a set.</p>
        <p><dfn id="def-selected">Selected item</dfn> can exist as a <strong>single or multiple instances</strong> in a set.</p>
        <p>Both indicate that the item in the collection has additional, contextual meaning or function due to user’s action or location.</p>
        <p>When in doubt or can't explicitly reason whether item should be considered as "current" or "selected" - always default to more generic "selected".</p>
    </div>

    <h2 id="behaviour">Behaviour</h2>

    <div class="aui-message aui-message-info" >
        <p>AUI provides markup and designs that have been tested to assure the most optimal available experience to all users. Whenever possible - use provided markup and styles.</p>
        <p>If dealing with custom styling or markup is a necessity - it's on you to guarantee that all the requirements mentioned in this design pattern are met.</p>
    </div>


    <h3>Current and Selected</h3>

    <p>Whether specifying current and/or selected elements, there are certain principles to follow:</p>

    <div class="aui-message aui-message-success" id="visually-identifiable">
        <p>
            Items have to be <strong>visually identifiable</strong> in a clear and distinguish way within the set.
        </p>
    </div>

    <p>The design has to guarantee that the items are distinguishable despite user' personal settings (think: font size, custom colour pallet, dark-mode, high-contrast mode etc…), color vision deficiencies or other vision impairment…</p>
    <figure>
        <img src="../images/a11y-color-vision-deficiency-design-issues.png" alt="Bad vs good example of visual design in context of color vision deficiency" />
        <figcaption>
            An example of exclusive and inclusive design based on horizontal navigation pattern.<br/>
            Regular user sees 3 types of items: regular (dark blue), current (lighter blue, bolded) and selected (lightest blue, framed).<br />
            User with Monochromacy sees 2 types of items: regular (grey), and selected (grey, framed). Current item looks like regular items.<br />
            User with Monochromacy + inclusive design = 3 types of items: regular (grey), current (grey, underlined), selected (grey, framed).
        </figcaption>
    </figure>
    <p><strong>AUI provides</strong> <code>aui-nav-selected</code> classname in navigation-pattern components (see: <a href="{{rootPath}}docs/navigation.html">navigation patterns</a>).</p>
    <p>Similar indicators may be provided for other, core components - always check component's documentation if adequate markup is available.</p>

    <div class="aui-message aui-message-success" id="tts-friendly">
        <p>
            Items have to <strong>provide a way to be recognisable by screen reader</strong> users and/or other assistive technologies.
        </p>
    </div>
    <p>
        Applying visual styling will not accommodate users with significant vision impairment or total vision loss. We may need to provide alternative means to make this information available - depending on context, markup or components used.<br />
        There are several means to achieve that:<br />
        <ul>
            <li>Certain form components provide adequate information out of the box - by usage of <code>checked</code> or <code>selected</code> attributes</li>
            <li><code>aria-selected</code> attribute may be added to other elements to explicitly state their role</li>
            <li>Using assistive labels - not visible to sighted users but read by screan readers may also provide adequate contextual information</li>
        </ul>
    </p>
    <p>
        <strong>AUI provides</strong> globally available <code>assistive</code> classname (see: <a href="{{rootPath}}docs/hidden-assistive-css.html">hidden assistive css</a>) allowing for labeling information that's not visible to sighted users but is properly read aloud by assistive technologies.
    </p>
    <p>
        Always include assistive label, unless component describes a preferred approach or provides a out-of-a-box solution, such as <code>checked</code>, <code>selected</code> or <code>aria-selected</code> attributes handled by the component.<br />
        Use natural language, appropriate in a given context, to provide adequate contextual information with the assistive label.<br />
        If your application allows user-selectable interface language - always provide translation for the label.
    </p>

    <p>
        While we generally recommend sticking to consistent approach across your application - adding contextual, assistive label may require more flexible approach due to particular language constructs.<br />
        As the label is due to follow the natural language flow - there might be more tha one ways to apply it in a given context.
    </p>

    <aui-docs-example id="horizontal-nav-example">
        <noscript is="aui-docs-code" type="text/html">
        <ul class="aui-nav">
            <li><a href="#new-item">Newest item</a></li>
            <li class="aui-nav-selected"><a href="#promo-item"><span class="assistive">Selected tab: </span>Promo item</a></li>
            <li><a href="#">Popular Item</a></li>
        </ul>
        <ul class="aui-nav">
            <li><a href="#new-item">Newest item</a></li>
            <li class="aui-nav-selected"><a href="#promo-item">Promo item <span class="assistive">(the selected tab)</span></a></li>
            <li><a href="#">Popular Item</a></li>
        </ul>
        </noscript>
    </aui-docs-example>

    <p>
        We generally follow the first presented approach in our examples, as - based on our experience - it is easier to handle when dealing with multi-language interfaces and translations. <br />
        Truth is, though - there exists no "best" way to approach this pattern.<br />
        Be aware of your target group, capabilities of your application and adjust according to own's needs.
    </p>

    <div class="aui-message aui-message-success" id="actionable">
        <p>
            Items have to <strong>be actionable</strong>.
        </p>
    </div>

    <p>Action may depend on current application's context:</p>
    <p>
        In a calendar pattern <b>selected</b> and non-selected items may allow for toggling between their state - allowing to easily "select" or "unselect" item within the collection.
        <b>Current</b> item - indicating currently set system date - may behave in the same manner as other selected items, though it's initial state might be different (the calendar component opens with "current" date selected).
    </p>
    <p>
        In a tabs pattern actioning on <b>highlighted</b> item may change the content of the landmark associated with the menu. It may also move the "current" marker onto the actioned item.<br />
        Actioning on <b>current</b> item may allow for easy move (content-jump) between the navigation menu and the landmark it controls by providing adequately targeted anchors.
    </p>
    <p>
        With the exception of a few corner cases - both selected and current items have to be actionable. Their actions may, and in many cases - will be the same.
    </p>

    <h3>Examples:</h3>

    <h4 id="horizontal">Horizontal navigation</h4>
    <p>
        Requirements: <strong>current element</strong> targets a landmark containing side and/or partial information, not fulfilling requirements of <a href="#current-page">current page</a><br />
        Notice: the current element has <code>aui-nav-selected</code> classname, provides adequate assistive label and is linked to an appropriate landmark</p>
    <aui-docs-example live-demo id="horizontal-nav-example">
        <noscript is="aui-docs-code" type="text/html">
            <nav class="aui-navgroup aui-navgroup-horizontal">
                <div class="aui-navgroup-inner">
                    <div class="aui-navgroup-primary">
                        <ul class="aui-nav">
                            <li><a href="#product-1">Product 1</a></li>
                            <li class="aui-nav-selected"><a href="#product-2"><span class="assistive">Selected item:</span> Product 2</a></li>
                            <li><a href="#product-3">Product 3</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </noscript>
    </aui-docs-example>

    <h4 id="tabs">Tabs panel</h4>
    <p>Notice: the current element has <code>aria-selected="true"</code> and is linked to an appropriate landmark</p>
    <aui-docs-example live-demo id="tabs-example">
        <noscript is="aui-docs-code" type="text/html">
            <div class="aui-tabs horizontal-tabs" id="tabs-example1" role="application">
                <ul class="tabs-menu" role="tablist">
                    <li class="menu-item active-tab" role="presentation">
                        <a href="#tabs-example-first" id="aui-uid-0" role="tab" aria-selected="true">Designers</a>
                    </li>
                    <li class="menu-item" role="presentation">
                        <a href="#tabs-example-second" id="aui-uid-1" role="tab" aria-selected="false">Developers</a>
                    </li>
                    <li class="menu-item" role="presentation">
                        <a href="#tabs-example-third" id="aui-uid-2" role="tab" aria-selected="false">PMs</a>
                    </li>
                </ul>
                <div class="tabs-pane active-pane" id="tabs-example-first" role="tabpanel" hidden aria-labelledby="aui-uid-0">
                    <h3>Designers</h3>
                    <table class="aui">
                        <thead>
                        <tr>
                            <th id="basic-number">#</th>
                            <th id="basic-fname">First name</th>
                            <th id="basic-lname">Last name</th>
                            <th id="basic-username">Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td headers="basic-number">1</td>
                            <td headers="basic-fname">Matt</td>
                            <td headers="basic-lname">Bond</td>
                            <td headers="basic-username">mbond</td>
                        </tr>
                        <tr>
                            <td headers="basic-number">2</td>
                            <td headers="basic-fname">Ross</td>
                            <td headers="basic-lname">Chaldecott</td>
                            <td headers="basic-username">rchaldecott</td>
                        </tr>
                        <tr>
                            <td headers="basic-number">3</td>
                            <td headers="basic-fname">Henry</td>
                            <td headers="basic-lname">Tapia</td>
                            <td headers="basic-username">htapia</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tabs-pane" id="tabs-example-second" role="tabpanel" hidden aria-labelledby="aui-uid-1">
                    <h3>Developers</h3>
                    <table class="aui">
                        <thead>
                        <tr>
                            <th id="basic-number">#</th>
                            <th id="basic-fname">First name</th>
                            <th id="basic-lname">Last name</th>
                            <th id="basic-username">Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td headers="basic-number">4</td>
                            <td headers="basic-fname">Seb</td>
                            <td headers="basic-lname">Ruiz</td>
                            <td headers="basic-username">sruiz</td>
                        </tr>
                        <tr>
                            <td headers="basic-number">7</td>
                            <td headers="basic-fname">Sean</td>
                            <td headers="basic-lname">Curtis</td>
                            <td headers="basic-username">scurtis</td>
                        </tr>
                        <tr>
                            <td headers="basic-number">8</td>
                            <td headers="basic-fname">Matthew</td>
                            <td headers="basic-lname">Watson</td>
                            <td headers="basic-username">mwatson</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tabs-pane" id="tabs-example-third" role="tabpanel" hidden aria-labelledby="aui-uid-2">
                    <h3>Product management</h3>
                    <table class="aui">
                        <thead>
                        <tr>
                            <th id="basic-number">#</th>
                            <th id="basic-fname">First name</th>
                            <th id="basic-lname">Last name</th>
                            <th id="basic-username">Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td headers="basic-number">5</td>
                            <td headers="basic-fname">Jens</td>
                            <td headers="basic-lname">Schumacher</td>
                            <td headers="basic-username">jschumacher</td>
                        </tr>
                        <tr>
                            <td headers="basic-number">6</td>
                            <td headers="basic-fname">Sten</td>
                            <td headers="basic-lname">Pittet</td>
                            <td headers="basic-username">spittet</td>
                        </tr>
                        <tr>
                            <td headers="basic-number">9</td>
                            <td headers="basic-fname">James</td>
                            <td headers="basic-lname">Dumay</td>
                            <td headers="basic-username">jdumay</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </noscript>

    </aui-docs-example>

    <h2>Unique cases</h2>
    <h3 id="current-page">Current page</h3>

    <p>
        By the current page we may understand an identifier such as url or name, that uniquely describes "where we are" within our application - similarly to "you are here" pointer on a map.<br/>
        "Current page" should describe and/or identify the content of the <code>&lt;main&gt;</code> landmark rendered at a given time.
    </p>

    <p>Always, when element of a set represents the current page, in navigation patterns or any other arbitrary collections</p>

    <div class="aui-message aui-message-warning">
        <p>
            Item <strong>should not be actionable</strong>, so users cannot interact with it.<br/>
        </p>
    </div>
    <p>
        The location you'd be referring to is already rendered and is the main content of the page the user sees.<br />
        As it's not representing any specific content, any particular landmark within the page - there's nowhere "to go to".
    </p>
    <p>
        This avoids misunderstandings and emphasises that the current list or menu item is active and represents the currently displayed content as a whole.
    </p>

    <h3>Example:</h3>

    <h4 id="current-in-breadcrumbs">Breadcrumbs</h4>
    <p>Notice: the <strong>current page</strong> has <code>aui-nav-selected</code> classname, provides adequate assistive label, is not a link.</p>

    <aui-docs-example live-demo id="breadcrumbs-example">
        <noscript is="aui-docs-code" type="text/html">
            <div class="aui-page-header-main">
                <ol class="aui-nav aui-nav-breadcrumbs">
                    <li><a href="https://example.com/">Breadcrumbs</a></li>
                    <li><a href="https://example.com/" class="custom">Sub-page</a></li>
                    <li class="aui-nav-selected"><span class="assistive">Current page: </span>Direct parent page name</li>
                </ol>
            </div>
        </noscript>
    </aui-docs-example>

    <h4 id="vertical">Vertical navigation</h4>
    <p>
        Notice: the <strong>current page</strong> has <code>aui-nav-selected</code> classname, provides adequate assistive label, is not a link.<br />
        Notice: due to unique styling of the component - additional <code>span</code> markup container is needed to encapsulate list's content for a non-interactive item.
    </p>
    <aui-docs-example live-demo id="vertical-nav-example">
        <noscript is="aui-docs-code" type="text/html">
            <nav class="aui-navgroup aui-navgroup-vertical">
                <div class="aui-navgroup-inner">
                    <ul class="aui-nav">
                        <li class="aui-nav-selected"><span><span class="assistive">Current page: </span>Page A</span></li>
                        <li><a href="https://example.com/">Page B</a></li>
                        <li><a href="https://example.com/">Page C</a></li>
                    </ul>
                </div>
            </nav>
        </noscript>
    </aui-docs-example>


    <h4 id="vertical">Vertical navigation (antipattern)</h4>
    <div class="aui-message aui-message-info" id="current-page-linked-antipattern">
        <h4>Current page with anchor element</h4>
        <p>It may happen that due to technical or business limitations removal of anchor elements from <b>current page</b> item is not desired or possible.</p>
        <p>Though we generally <strong>do not recommend such behaviour</strong> - certain steps have to be undertaken to adequately assist users in such scenario:<br />
        <ul>
            <li>Item has to <strong>include <code>aria-current="page"</code></strong> attribute in it's anchor</li>
            <li>Item <strong>cannot</strong> include custom, <strong>assistive label</strong></li>
        </ul>
        </p>
    </div>

    <p>
        Requirements: Due to business requirements <strong>current page</strong> has to be linked<br />
        Notice: it has <code>aui-nav-selected</code> classname, the anchored element has <code>aria-current="page"</code> attribute added and <b>no assistive label</b> is provided
    </p>
    <aui-docs-example live-demo id="vertical-nav-example">
        <noscript is="aui-docs-code" type="text/html">
            <nav class="aui-navgroup aui-navgroup-vertical">
                <div class="aui-navgroup-inner">
                    <ul class="aui-nav">
                        <li class="aui-nav-selected"><a href="https://example.com/" aria-current="page">Page A</a></li>
                        <li><a href="https://example.com/">Page B</a></li>
                        <li><a href="https://example.com/">Page C</a></li>
                    </ul>
                </div>
            </nav>
        </noscript>
    </aui-docs-example>

    <footer class="footnotes">
        <h4 id="footnotes">Footnotes</h4>
        <p id="fn1">
            <a href="#r1"> [1]</a> <strong>groups</strong>, <strong>sets</strong> and <strong>collections</strong> have similar, though semantically different meanings.<br />
            <dfn>Groups</dfn>, also known as <dfn>sets</dfn>, usually represent "buckets of items" which do not imply direct relation between each of it's items. Similar to a basket of apples - it's there just to containerise its content. In HTML <code>div</code> would be a typical example of group encapsulating element.<br />
            <dfn>Collections</dfn> are special "sets of items" which usually implies some kind of relation between its items. Similar to a postage stamp booklet - it groups it's content according to a given relation (value, year, occasion). In HTML <code>ul</code> would be a typical example of collection encapsulating element.<br />
            One could say that groups allow for containerisation with "weak relation" whereas collections - with "strong relations".<br />
            For the sake of this documentation, though, we'll use these terms interchangeably - meaning any or all of the above.
        </p>
        <p id="fn2">
            <a href="#r2"> [2]</a> <strong>active, selected, highlighted</strong> for the sake of this documentation we'll use those terms interchangeably
        </p>
    </footer>
</article>
