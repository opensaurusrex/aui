import './aui.page.reset';
import '../internal/iconfont'; // todo: should only import the icons it needs
import './aui.pattern.icon';
import '@atlassian/aui/src/less/adg-iconfont.less';
import '@atlassian/aui/src/less/adg-missing-icons.less';
import '@atlassian/aui/src/less/adgs-icons.less';
