import './styles/aui.pattern.avatar';
import '@atlassian/aui/src/less/aui-avatars.less';
import { AvatarGroupEl } from '@atlassian/aui/src/js/aui/avatar-group';
export { AvatarGroupEl };
