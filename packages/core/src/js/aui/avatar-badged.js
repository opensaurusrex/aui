import skate from './internal/skate';

const DEFAULT_VALUE = 'bottom-end'

const setBadgedPlacement = (element, newValue, oldValue) => {
    const placementOptions = ['top-start', 'top-end', 'bottom-start', 'bottom-end'];
    const newPlacementClass = placementOptions.includes(newValue) ? newValue : DEFAULT_VALUE;

    if (oldValue !== null) {
        element.classList.remove(`aui-avatar-badged-${oldValue}`);
    }
    element.classList.add(`aui-avatar-badged-${newPlacementClass}`);
}

const AvatarBadged = skate('aui-avatar-badged', {
    attached(element) {
        const value = element.getAttribute('placement');
        if (value.length) {
            setBadgedPlacement(element, value);
        }
    },

    attributes: {
        placement: {
            value: DEFAULT_VALUE,

            fallback: function(element, { newValue, oldValue}) {
                setBadgedPlacement(element, newValue, oldValue);
            },
        }
    },

    created: function(element) {
        element.className = 'aui-avatar-badged';
    }
});

export { AvatarBadged };
