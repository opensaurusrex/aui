// Global setup that used to be in the main AJS namespace file.
import $ from './jquery';
import globalize from './internal/globalize';
import populateParameters from './populate-parameters';
import version from './version';

// Set the version.
$(function () {
    var $body = $('body');

    if (!$body.data('auiVersion')) {
        $body.attr('data-aui-version', version);
    }

    populateParameters();
});

globalize('$', $);
export default $;
