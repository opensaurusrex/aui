/**
 * @fileOverview This script is used to generate `bundlesize` config files.
 *
 * It is used by package.json bundlesize/raw and bundlesize/gzipped scripts.
 *
 * It supports one CLI param - called with `raw` option will generate config
 * which uses ungzipped file sizes to run `bundlesize`.
 *
 * Eg.
 * `generate-bundlesize-config raw`
 */

const args = process.argv.slice(2);
const isRaw = args[0] === 'raw';

const files = require('./dependencies/files-with-sizes');

const rawBundleSizes = Object.entries(files).map(([path, [raw, gzipped]]) => ({
    path: 'dist/aui/' + path,
    maxSize: (isRaw ? raw : gzipped).toFixed(2) + ' kB',
    compression: isRaw ? 'none' : undefined
}));

console.log(JSON.stringify({ files: rawBundleSizes }));
