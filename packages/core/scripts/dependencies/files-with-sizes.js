/**
 * @fileOverview The exported object represents filepaths and sizes used by bundlesize scripts.
 *
 * - The keys are the filepaths (`packages/core/dist/aui/` prefix is stripped here for brevity)
 * - The values represent pairs of [a, b] where
 *   - `a` is a raw file size in kB
 *   - `b` is a gzipped file size in kB
 *
 * The file sizes below are rounded up by only 0.01 kB from existing sizes
 * so that if any one of them grows significantly we will know.
 */

module.exports = {
    'aui-css-deprecations.js': [12.3, 3.85],
    'aui-prototyping.css': [260.0, 42.0],
    'aui-prototyping.js': [456.0, 135.0],
    'aui-prototyping.nodeps.css': [260.0, 42.0],
    'aui-prototyping.nodeps.js': [412.0, 119.90],
    'aui-prototyping-darkmode.css': [12.32, 2.15],
    'aui-prototyping-browserfocus.css': [13.69, 2.71],

    'fonts/adgs-icons.eot': [60.09, 22.62],
    'fonts/adgs-icons.ttf': [59.91, 22.56],
    'fonts/adgs-icons.woff': [44.93, 19.97],
    'fonts/atlassian-icons.eot': [55.07, 23.03],
    'fonts/atlassian-icons.ttf': [54.85, 22.93],
    'fonts/atlassian-icons.woff': [105.67, 46.57],

    'images/adgs-icons.svg': [624.29, 103.9],
    'images/atlassian-icons.svg': [105.21, 31.14],
};
